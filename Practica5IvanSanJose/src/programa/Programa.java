package programa;

import java.util.Scanner;

import clases.Juego;

public class Programa {
	
	public static void main(String[] args) {
	
	Scanner sc = new Scanner(System.in);
	
	System.out.println("1- Creamos la instancia de Juego");
	Juego juego = new Juego();
	
	System.out.println("2- Damos de alta un Piloto, comprobando si existe previamente (3 pilotos minimo)");
	juego.altaPiloto();
	
	System.out.println("3- Listamos todos los Pilotos disponibles");
	juego.listaPilotos();
	
	System.out.println("4- Buscamos un piloto de rango que queramos");
	String rango = sc.nextLine();
	System.out.println(juego.buscarPiloto(rango));
	
	System.out.println("5- Eliminamos un piloto con nombre que queramos");
	System.out.println("�Que piloto quieres eliminar");
	String nombre = sc.nextLine();
	juego.eliminarPiloto(nombre);
	juego.listaPilotos();
	
	System.out.println("6- Damos de alta una nave, comprobando si existe previamente (3 naves minimo)");
	juego.altaNave();
	juego.listaNaves();
	
	System.out.println("7- Eliminamos una nave segun su modelo");
	System.out.println("�Que nave quieres eliminar?");
	String modelo = sc.nextLine();
	juego.eliminarNave(modelo);
	juego.listaNaves();
	
	System.out.println("8- Buscamos una nave por su nombre");
	String nombreNave = sc.nextLine();
	System.out.println(juego.buscarNave(nombreNave));
	
	System.out.println("9- Listamos todas la naves con la capacidad deseada");
	int capacidad = sc.nextInt();
	juego.listarNaveCapacidad(capacidad);
	sc.nextLine();
	
	System.out.println("10- Asignamos un piloto a una nave");
	System.out.println("Nombre del piloto y nombre de la nave");
	String nombrePiloto = sc.nextLine();
	String nombreNave1 = sc.nextLine();
	juego.asignarPiloto(nombrePiloto, nombreNave1);
	
	System.out.println("11- Listamos las naves que sean propiedad de un piloto");
	String navePiloto = sc.nextLine();
	juego.listarNavesDePiloto(navePiloto);
	juego.listaNaves();
	
	System.out.println("12- Ordenar por orden alfabetico las naves");
	juego.ordenarNaves();
	
	sc.close();
	}

}
