package clases;

import java.util.Scanner;

public class Piloto {

	Scanner input = new Scanner(System.in);	
	
	//atributos
	private String nombre;
	private int edad;
	private String rango;
	private String tipoArma;
	private String planetaNacimiento;
	
	//constructor
	public Piloto(String nombre, int edad, String rango, String tipoArma, String planetaNacimiento) {
		this.nombre = nombre;
		this.edad = edad;
		this.rango = rango;
		this.tipoArma = tipoArma;
		this.planetaNacimiento = planetaNacimiento;
	}
	public Piloto() {
		this.nombre = "";
		this.edad = 0;
		this.rango = "";
		this.tipoArma = "";
		this.planetaNacimiento = "";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getRango() {
		return rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

	public String getTipoArma() {
		return tipoArma;
	}

	public void setTipoArma(String tipoArma) {
		this.tipoArma = tipoArma;
	}

	public String getPlanetaNacimiento() {
		return planetaNacimiento;
	}

	public void setPlanetaNacimiento(String planetaNacimiento) {
		this.planetaNacimiento = planetaNacimiento;
	}
	
	@Override
	public String toString() {
		return "Piloto [nombre=" + nombre + ", edad=" + edad + ", rango=" + rango + ", tipoArma=" + tipoArma
				+ ", planetaNacimiento=" + planetaNacimiento + "]";
	}
	
	public void llenarPiloto() {
		System.out.println("Introduce los datos del piloto");
		System.out.println("Nombre, edad, rango, tipo de arma y planeta de nacimiento respectivamente");
		this.nombre = input.next();
		this.edad = input.nextInt();
		this.rango = input.next();
		this.tipoArma = input.next();
		this.planetaNacimiento = input.next();
	}
	
}
