package clases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class Juego {

	Scanner input = new Scanner(System.in);
	
	//atributos
	private ArrayList<Piloto> listaPilotos;
	private ArrayList<Nave> listaNaves;
	
	//constructor
	public Juego() {
		listaPilotos = new ArrayList<Piloto>();
		listaNaves = new ArrayList<Nave>();
	}
	
	//metodo existePiloto
	public boolean existePiloto(String nombre) {
		for(Piloto piloto: listaPilotos) {
			if(piloto != null && piloto.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}
	
	
	//metodo altaPiloto
	public void altaPiloto() {
		String respuesta = "";
		do {
			Piloto nuevoPiloto = new Piloto();
			nuevoPiloto.llenarPiloto();
			listaPilotos.add(nuevoPiloto);
			System.out.println("�Quiere introducir otro Piloto? (si/no)");
			respuesta = input.nextLine();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	//metodo listarPilotos
	public void listaPilotos() {
		for(Piloto piloto: listaPilotos) {
			if(piloto != null) {
				System.out.println(piloto);
			}
		}
	}
	
	//metodo buscarPiloto
	public Piloto buscarPiloto(String rango) {
		for(Piloto piloto1 : listaPilotos) {
			if(piloto1 != null && piloto1.getRango().equals(rango)) {
				return piloto1;
			}
		}
		return null;
		}
	
	//metodo eliminarPiloto
	public void eliminarPiloto(String nombre) {
		Iterator<Piloto> itPiloto = listaPilotos.iterator();
			
		while(itPiloto.hasNext()) {
			Piloto piloto = itPiloto.next();
			if(piloto.getNombre().equals(nombre)) {
					itPiloto.remove();
				}
			}
		}
	
	//metodo existeNave
		public boolean existeNave(String nombre) {
			for(Nave nave: listaNaves) {
				if(nave != null && nave.getNombre().equals(nombre)) {
					return true;
				}
			}
			return false;
		}
		
	//metodo altaNave
	public void altaNave() {
		String respuesta = "";
		do {
			Nave nuevaNave = new Nave();
			nuevaNave.llenarNave();
			listaNaves.add(nuevaNave);
			System.out.println("�Quiere introducir otra Nave? (si/no)");
			respuesta = input.nextLine();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	//metodo listarNaves
	public void listaNaves() {
		for(Nave nave: listaNaves) {
			if(nave != null) {
				System.out.println(nave);
			}
		}
	}
	
	//metodo eliminar Nave
	public void eliminarNave(String modelo) {
		Iterator<Nave> itNave = listaNaves.iterator();
		
		while(itNave.hasNext()) {
			Nave nave = itNave.next();
			if(nave.getNombre().equals(modelo)) {
				itNave.remove();
			}
		}
	}
	
	//metodo buscarNave
	public Nave buscarNave(String nombreNave) {
		for(Nave nave : listaNaves) {
			if(nave != null && nave.getNombre().equals(nombreNave)) {
				return nave;
			}
		}
		return null;
	}
	
	//metodo listarNave por capacidad
	public void listarNaveCapacidad(int capacidad) {
		for(Nave nave : listaNaves) {
			if(nave.getCapacidad() == capacidad) {
				System.out.println(nave);
			}
		}
	}
		
	//metodo listar elemento de nave con uno de piloto
	public void listarNavesDePiloto(String navePiloto) {
		for(Nave nave : listaNaves) {
			if(nave.getPilotoNave() != null && nave.getPilotoNave().getNombre().equals(navePiloto)) {
				System.out.println(nave);
			}
		}
	}
	
	//metodo asignarPiloto a nave
	public void asignarPiloto(String nombrePiloto, String nombreNave1) {
		if(buscarPiloto(nombrePiloto) != null && buscarNave(nombreNave1) != null) {
			Piloto piloto1 = buscarPiloto(nombrePiloto);
			Nave nave = buscarNave(nombreNave1);
			nave.setPilotoNave(piloto1);
		}
	}
	
	//metodo a�adido ordenarNaves
	public void ordenarNaves() {
		for(int i = 0; i < listaNaves.size(); i++) {
			for(int j = i+1; j < listaNaves.size();i++) {
				if(listaNaves.get(i).getNombre().compareTo(listaNaves.get(j).getNombre())>0) {
					Nave orden = listaNaves.get(i);
					listaNaves.set(i, listaNaves.get(j));
					listaNaves.set(j, orden);
				}
			}
		}
		System.out.println("Ya esta ordenada la lista");
	}
	
}
