package clases;

import java.time.LocalDate;
import java.util.Scanner;

public class Nave {

	Scanner input = new Scanner(System.in);
	
	//atributos
	private String nombre;
	private String modelo;
	private int capacidad;
	private boolean viajeVelocidadLuz;
	private LocalDate fechaConstruccion;
	private Piloto pilotoNave;
	
	//constructor
	public Nave(String nombre, String modelo, int capacidad, boolean viajeVelocidadLuz) {
		this.nombre = nombre;
		this.modelo = modelo;
		this.capacidad = capacidad;
		this.viajeVelocidadLuz = viajeVelocidadLuz;
	}
	
	public Nave() {
		this.nombre = "";
		this.modelo = "";
		this.capacidad = 0;
		this.viajeVelocidadLuz = true;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public boolean isViajeVelocidadLuz() {
		return viajeVelocidadLuz;
	}

	public void setViajeVelocidadLuz(boolean viajeVelocidadLuz) {
		this.viajeVelocidadLuz = viajeVelocidadLuz;
	}

	public LocalDate getFechaConstruccion() {
		return fechaConstruccion;
	}

	public void setFechaConstruccion(LocalDate fechaConstruccion) {
		this.fechaConstruccion = fechaConstruccion;
	}

	public Piloto getPilotoNave() {
		return pilotoNave;
	}

	public void setPilotoNave(Piloto pilotoNave) {
		this.pilotoNave = pilotoNave;
	}

	@Override
	public String toString() {
		return "Nave [nombre=" + nombre + ", modelo=" + modelo + ", capacidad=" + capacidad + ", viajeVelocidadLuz="
				+ viajeVelocidadLuz + ", fechaConstruccion=" + fechaConstruccion + ", pilotoNave=" + pilotoNave + "]";
	}

	public void llenarNave() {
		System.out.println("Introduce los datos de la nave");
		System.out.println("Nombre, modelo, capacidad y si viaja a la velocidad de la luz o no respectivamente");
		this.nombre = input.next();
		this.modelo = input.next();
		this.capacidad = input.nextInt();
		this.viajeVelocidadLuz = input.nextBoolean();
	}
	
}
